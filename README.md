# Software Heritage's FAQ repository

This repository is dedicated to writing the collection of Frequently Asked Questions.
Ambassadors and contributors can do MR or add issues dedicated to the FAQ.
- the faq.md file can be easily exported into html with the correct tags per question
- concerning the devs/users/sys-admins FAQ, they will be directly edited in the docs
- if issues on the FAQ repository will concern devs/users/sys-admins
- these issues will be tagged and resolved with addition on the docs instance

We have now a significant amount of material from interviews, newspaper articles, videos, presentations etc.
From this material, we compiled a list of frequently asked questions with the typical answers we provide.
E.g.: what is software source code? why archiving software source code? how do you decide what to archive? do you also archive binaries? hey, the Library of Alexandria went up in flames, how do you plan to avoid this happening to you? etc. etc.

The full FAQ, with some unpublished content waiting for review, is available on [faq.md](faq.md) while the published FAQ is available on [20240701_faq.md](20240701_faq.md), section 1 to 5 are already online on https://www.softwareheritage.org/faq/.

The content of each FAQ page (one per supported language) located at https://www.softwareheritage.org/faq/ is dynamically generated from associated markdown file in that repository, every change pushed to is automatically taken into account when refreshing the published FAQ page.
